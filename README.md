# LoL-Game Play Element Detection

| zoomed screen | screen | analysis output | zoomed output |
| ------ | ------ | ------ | ------ |
| ![Team View](https://gitlab.com/isgursoy/lol-game-play-element-detection/-/raw/master/cropped1original.png "Teams") | ![Team View](https://gitlab.com/isgursoy/lol-game-play-element-detection/-/raw/master/screen1.png "Teams") | ![Team View](https://gitlab.com/isgursoy/lol-game-play-element-detection/-/raw/master/1-output.png "Teams") | ![Team View](https://gitlab.com/isgursoy/lol-game-play-element-detection/-/raw/master/cropped1processed.png "Teams") |
| ![Minimap View](https://gitlab.com/isgursoy/lol-game-play-element-detection/-/raw/master/cropped2original.png "Minimap") | ![Minimap View](https://gitlab.com/isgursoy/lol-game-play-element-detection/-/raw/master/screen2.png "Minimap") | ![Minimap View](https://gitlab.com/isgursoy/lol-game-play-element-detection/-/raw/master/2-output.png "Minimap") | ![Minimap View](https://gitlab.com/isgursoy/lol-game-play-element-detection/-/raw/master/cropped2processed.png "Minimap") |

## Teams
The task is to recognize which champions the two teams have. You can assume that the locations of champion icons are static and each champion has only one icon.

## Locating Players on Minimap
Assuming you already found the names of all 10 champions in that match by performing the Required tasks, locate of the champion icons on the minimap at the lower right corner of the screen and indicate their positions by either drawing bounding boxes or printing coordinates.

## Usage:
Code works for any of screen1.png and screen2.png for both of team and minimap tasks.

```lol.py --image screen1.png --assets-directory champions```
