# pip:
# psutil
# opencv-contrib-python
# numpy
import numpy as np
import psutil
import argparse
import cv2 as cv
import os
import multiprocessing
from functools import partial

debug = False

class LolParser:
	def __init__(this, championsFolderPath):
		this.__prepareChampionsBook(championsFolderPath)

	def __prepareChampionsBook(this, folderPath):
		this.championsBook = []
		this.playersBook = []
		for (_, _, files) in os.walk(folderPath):
			for filename in files:
				basename = os.path.splitext(filename)[0]
				image = cv.imread(folderPath + "/" + filename)
				this.championsBook.append((basename, image))

	def go(this, screenShot):
		return this.learnTeams(screenShot)

	def learnTeams(this, screenShot):
		avatarEdgeOnScreen = 40
		# roi is in 30,150-1890,640 as left 30,150-75.640 and right 1845,150-1890,640
		teams = {"left": np.s_[150:640, 30:30 + avatarEdgeOnScreen],
				 "right": np.s_[150:640, 1845:1845 + avatarEdgeOnScreen]}
		padding = int(avatarEdgeOnScreen / 2)
		bufferHeight = 640 - 150 + (2 * padding)
		bufferWidth = (avatarEdgeOnScreen * 3) + (2 * padding)
		teamsSection = np.zeros((bufferHeight, bufferWidth, 3), np.uint8)
		teamsSection[padding:bufferHeight - padding, padding:padding + avatarEdgeOnScreen] = screenShot[
			teams["left"]].copy()
		teamsSection[padding:bufferHeight - padding, bufferWidth - padding - avatarEdgeOnScreen:bufferWidth - padding] = \
			screenShot[
				teams["right"]].copy()
		processingScale = 3
		screenToDrawResult = screenShot.copy()
		screenRGB = cv.resize(teamsSection,
							  (teamsSection.shape[1] * processingScale, teamsSection.shape[0] * processingScale))
		screenGray = cv.cvtColor(screenRGB, cv.COLOR_BGR2GRAY)
		screenCannied = cv.Canny(screenGray, 50, 200)
		templateEdge = avatarEdgeOnScreen * processingScale
		if debug:
			cv.imshow("screen", screenRGB)
		coreCount = psutil.cpu_count(logical=True)
		pool = multiprocessing.Pool(processes=coreCount)
		argPack = partial(this.convolutionWorker, screenRgbImage=screenRGB, screenCanniedImage=screenCannied,
						  templateSize=templateEdge,
						  scale=padding * processingScale)
		convolutionSimilarityResult = pool.map(argPack, this.championsBook)
		cv.putText(screenToDrawResult, "Left Team", (20, 20), cv.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 1, cv.LINE_AA)
		cv.putText(screenToDrawResult, "Right Team", (screenToDrawResult.shape[1] - 210, 20), cv.FONT_HERSHEY_SIMPLEX,
				   1,
				   (0, 0, 255), 1, cv.LINE_AA)
		for result in convolutionSimilarityResult:
			name, teamSide, convolutionProposal, convolutionScore = result
			if convolutionProposal is None:
				continue
			this.playersBook.append(this.__getChampionByName(name))
			cv.putText(screenToDrawResult, name,
					   (convolutionProposal[1] + 10, convolutionProposal[2]) if teamSide == "left" else (
						   convolutionProposal[0] - 60, convolutionProposal[2]), cv.FONT_HERSHEY_SIMPLEX,
					   0.5, (255, 255, 0), 1, cv.LINE_AA)
			cv.rectangle(screenToDrawResult, (convolutionProposal[0], convolutionProposal[2]),
						 (convolutionProposal[1], convolutionProposal[3]), color=(255, 255, 255), thickness=2)
		return this.locatePlayers(screenToDrawResult)

	def convolutionWorker(this, template, screenRgbImage, screenCanniedImage, templateSize, scale):
		name, championRGB = template
		if scale == 1:
			championRGB = cv.resize(championRGB,
									(championRGB.shape[1] / 3, championRGB.shape[0] / 3))
		convolutionProposal, convolutionScore = this.__runSlidingConvolutionWindow(screenRgbImage,
																				   screenCanniedImage,
																				   championRGB)
		teamSide = None
		if convolutionProposal is not None:
			convolutionProposal, teamSide = this.__translateOutputRectangleAndGetTeam(False,
																					  convolutionProposal,
																					  templateSize, scale)
		return name, teamSide, convolutionProposal, convolutionScore

	def locatePlayers(this, screenShot):
		# 1700,860-1910,1070 in bonus 1655,815-1907,1066 in main
		miniMap = screenShot[815:1070, 1655:1910].copy()
		playerEdgeOnScreen = 25
		processingScale = 5
		templateEdge = playerEdgeOnScreen * processingScale
		screenToDrawResult = screenShot.copy()
		minimapRGB = cv.resize(miniMap,
							   (miniMap.shape[1] * processingScale, miniMap.shape[0] * processingScale))
		if debug:
			cv.imshow("map", minimapRGB)
		coreCount = psutil.cpu_count(logical=True)
		pool = multiprocessing.Pool(processes=coreCount)
		argPack = partial(this.colorSimilarityWorker, screen=minimapRGB, threshold=0.5, scale=processingScale,
						  templateSize=templateEdge)
		colorSimilarityResults = pool.map(argPack, this.playersBook)
		toBeEliminated = []
		for rectPack in colorSimilarityResults:
			nameOfRect, _, rect, score = rectPack
			if rect is None:
				toBeEliminated.append(rectPack)
				continue
			for anotherRectPack in colorSimilarityResults:
				nameOfAnotherRect, _, anotherRect, anotherScore = anotherRectPack
				if anotherRect is None:
					toBeEliminated.append(anotherRectPack)
					continue
				if rectPack == anotherRectPack:
					continue
				if this.__calculateIntersection(rect, anotherRect) > (
						0.7 * max(this.__getArea(rect), this.__getArea(anotherRect))):
					toBeEliminated.append(anotherRectPack if score > anotherScore else rectPack)
		colorSimilarityResults = [elem for elem in colorSimilarityResults if elem not in toBeEliminated]
		for result in colorSimilarityResults:
			name, _, colorSimilarityProposal, bestColorScoreSoFar = result
			cv.rectangle(screenToDrawResult, (colorSimilarityProposal[0], colorSimilarityProposal[2]),
						 (colorSimilarityProposal[1], colorSimilarityProposal[3]),
						 color=(255, 0, 0), thickness=2)
		return screenToDrawResult

	def colorSimilarityWorker(this, template, screen, threshold, scale, templateSize):
		name, championRGB = template
		colorSimilarityProposal, bestColorScoreSoFar = this.__runSlidingColorSimilarityWindow(screen,
																							  championRGB,
																							  threshold,
																							  scale)
		teamSide = None
		if colorSimilarityProposal is not None:
			colorSimilarityProposal, teamSide = this.__translateOutputRectangleAndGetTeam(True,
																						  colorSimilarityProposal,
																						  templateSize)
		return name, teamSide, colorSimilarityProposal, bestColorScoreSoFar

	def __getChampionByName(this, name):
		for champPack in this.championsBook:
			id, champ = champPack
			if name == id:
				return champPack

	def __getArea(this, rect):
		return (rect[1] - rect[0]) * (rect[3] - rect[2])

	def __translateOutputRectangleAndGetTeam(this, minimap, rect, processedTemplateEdge, padding=0):
		# x1,x2,y1,y2
		scale = 5
		if minimap:
			x1 = 1655 + int(rect[0] / scale)
			x2 = x1 + 25
			y1 = 815 + int(rect[2] / scale)
			y2 = y1 + 25
			return [x1, x2, y1, y2], None
		scale = 3
		team = "right"
		if rect[0] < processedTemplateEdge * 1.5:
			team = "left"
			return [30, 70, 150 + int((rect[2] - padding) / scale), 150 + int((rect[2] - padding) / scale) + 40], team
		return [1845, 1885, 150 + int((rect[2] - padding) / scale), 150 + int((rect[2] - padding) / scale) + 40], team

	def __runSlidingColorSimilarityWindow(this, screenRGB, templateRgb, threshold=0.3, processingScale=5, padding=0,
										  windowStride=15):
		templateEdge = templateRgb.shape[0]
		screenWidth = screenRGB.shape[1]
		screenHeight = screenRGB.shape[0]
		bins = 8
		histogramSize = 256
		bestProposal = None
		bestProposalScore = -1
		templateHist = cv.calcHist(
			[templateRgb],
			[0, 1, 2], None, [bins, bins, bins],
			[0, histogramSize, 0, histogramSize, 0, histogramSize]).flatten()
		xRangeBegin = padding * processingScale
		xRangeEnd = screenWidth
		yRangeBegin = padding * processingScale
		yRangeEnd = screenHeight
		for searchWindowX in range(xRangeBegin, xRangeEnd, windowStride):
			for searchWindowY in range(yRangeBegin, yRangeEnd, windowStride):
				regionProposalHist = cv.calcHist(
					[screenRGB[
						 np.s_[searchWindowY:searchWindowY + templateEdge,
						 searchWindowX:searchWindowX + templateEdge]]],
					[0, 1, 2], None, [bins, bins, bins],
					[0, histogramSize, 0, histogramSize, 0, histogramSize]).flatten()
				similarityScore = (1 - cv.compareHist(templateHist, regionProposalHist, cv.HISTCMP_BHATTACHARYYA))
				if debug:
					cv.imshow("score: ", screenRGB[
						np.s_[searchWindowY:searchWindowY + templateEdge,
						searchWindowX:searchWindowX + templateEdge]])
					cv.waitKey(1)
				if similarityScore >= threshold:
					if similarityScore > bestProposalScore:
						bestProposal = [searchWindowX, searchWindowX + templateEdge, searchWindowY,
										searchWindowY + templateEdge]
						bestProposalScore = similarityScore
		if debug:
			if bestProposal is not None:
				cv.imshow("Histogram matching Score: " + str(bestProposalScore),
						  screenRGB[bestProposal[2]:bestProposal[3], bestProposal[0]:bestProposal[1]])
				cv.waitKey()
		return bestProposal, bestProposalScore

	def __runSlidingKeypointMatchingWindow(this, screenRGB, templateRgb):
		pass

	def __runSlidingConvolutionWindow(this, screenRGB, screenCannied, templateRgb):
		methods = ['cv.TM_CCOEFF_NORMED', 'cv.TM_CCORR_NORMED']
		intersectionOfFirstProposal = []
		proposalScore = 0
		for methodString in methods:
			method = eval(methodString)
			templateGray = cv.cvtColor(templateRgb, cv.COLOR_BGR2GRAY)
			templateCannied = cv.Canny(templateGray, 50, 200)
			(templateHeight, templateWidth) = templateRgb.shape[:2]
			resultRGB = cv.matchTemplate(screenRGB, templateRgb, method)
			(_, maxValRGB, _, maxLocRGB) = cv.minMaxLoc(resultRGB)
			resultCannied = cv.matchTemplate(screenCannied, templateCannied, method)
			(_, maxValCannied, _, maxLocCannied) = cv.minMaxLoc(resultCannied)
			rgbProposalRect = [maxLocRGB[0], maxLocRGB[0] + templateWidth,
							   maxLocRGB[1], maxLocRGB[1] + templateWidth]
			canniedProposalRect = [maxLocCannied[0], maxLocCannied[0] + templateWidth,
								   maxLocCannied[1], maxLocCannied[1] + templateWidth]
			if debug:
				cv.imshow("template", templateRgb)
				cv.imshow("rgbMatch:" + methodString, screenRGB[maxLocRGB[1]:maxLocRGB[1] + templateWidth,
													  maxLocRGB[0]:maxLocRGB[0] + templateWidth])
				cv.imshow("canniedMatch:" + methodString,
						  screenRGB[maxLocCannied[1]:maxLocCannied[1] + templateWidth,
						  maxLocCannied[0]:maxLocCannied[0] + templateWidth])
				cv.waitKey()
			if methodString == methods[0]:
				intersectionOfFirstProposal = this.__getIntersection(rgbProposalRect, canniedProposalRect)
				proposalScore = max(proposalScore, max(maxValRGB, maxValCannied))
				if intersectionOfFirstProposal is None:
					return None, 0
			else:
				intersectionOfSecondProposal = this.__getIntersection(rgbProposalRect, canniedProposalRect)
				if intersectionOfSecondProposal is None:
					return None, 0
				intersectionOfFirstProposal = this.__getIntersection(intersectionOfFirstProposal,
																	 intersectionOfSecondProposal)
				proposalScore = max(proposalScore, max(maxValRGB, maxValCannied))
		return intersectionOfFirstProposal, proposalScore

	def __verifyMatchByColorSimilarity(this, claimedScreenRegion, templateRgb):
		bins = 8
		histogramSize = 64
		templateHist = cv.calcHist(
			[templateRgb],
			[0, 1, 2], None, [bins, bins, bins],
			[0, histogramSize, 0, histogramSize, 0, histogramSize]).flatten()
		claimedScreenRegionHist = cv.calcHist(
			[claimedScreenRegion],
			[0, 1, 2], None, [bins, bins, bins],
			[0, histogramSize, 0, histogramSize, 0, histogramSize]).flatten()
		return (1 - cv.compareHist(templateHist, claimedScreenRegionHist, cv.HISTCMP_BHATTACHARYYA))

	def __verfyMatchByKeypointMatching(this, claimedScreenRegion, templateRgb):
		pass

	def __getIntersection(this, rect1, rect2):
		if this.__calculateIntersection(rect1, rect2) < (max(this.__getArea(rect1), this.__getArea(rect1)) * 0.9):
			return None
		l1 = rect1[0]
		l2 = rect2[0]
		r1 = rect1[1]
		r2 = rect2[1]
		t1 = rect1[2]
		t2 = rect2[2]
		b1 = rect1[3]
		b2 = rect2[3]

		return [max(l1, l2), max(t1, t2), min(r1, r2), min(b1, b2)]

	def __calculateIntersection(this, rect1, rect2):
		l1 = rect1[0]
		l2 = rect2[0]
		r1 = rect1[1]
		r2 = rect2[1]
		t1 = rect1[2]
		t2 = rect2[2]
		b1 = rect1[3]
		b2 = rect2[3]
		hIntersection = 0
		vIntersection = 0

		if l1 >= l2 and r1 <= r2:  # Contained
			hIntersection = r1 - l1
		elif l1 < l2 and r1 > r2:  # Contains
			hIntersection = r2 - l2
		elif l1 < l2 and r1 > l2:  # Intersects right
			hIntersection = r1 - l2
		elif r1 > r2 and l1 < r2:  # Intersects left
			hIntersection = r2 - l1
		else:  # No intersection (either side)
			hIntersection = 0

		if not hIntersection:
			return 0

		if t1 >= t2 and b1 <= b2:  # Contained
			vIntersection = b1 - t1
		elif t1 < t2 and b1 > b2:  # Contains
			vIntersection = b2 - t2
		elif t1 < t2 and b1 > t2:  # Intersects right
			vIntersection = b1 - t2
		elif b1 > b2 and t1 < b2:  # Intersects left
			vIntersection = b2 - t1
		else:  # No intersection (either side)
			vIntersection = 0

		return hIntersection * vIntersection

def main():
	ap = argparse.ArgumentParser()
	ap.add_argument("-i", "--image", type=str,
					help="path to input image")
	ap.add_argument("-a", "--assets-directory", type=str,
					help="path to champions directory")
	args = vars(ap.parse_args())

	imagePath = args["image"]
	assetsDirectory = args["assets_directory"]

	image = cv.imread(imagePath)
	parser = LolParser(assetsDirectory)
	cv.imshow("Result", parser.go(image))
	cv.waitKey()

	print("Done.")


if __name__ == '__main__':
	main()
